#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <numeric>

using matrix = std::vector<std::vector<float>>;

class Problem {
	size_t n, m;
	std::vector<float> c;
	size_t rows, cols;
	std::vector<std::vector<float>> M;
public:
	Problem();
	Problem(std::string& file_path);
	~Problem();

	size_t get_n(void) const;
	size_t get_m(void) const;
	const matrix& get_M(void) const;
	size_t get_cols(void) const;
	size_t get_rows(void) const;

	float cost(size_t a, size_t b) const;
	std::vector<size_t> sort_by_cost(void) const;
	float calc_cost(const std::vector<size_t>& indices) const;
	std::vector<size_t> routing(const std::vector<size_t>& gateways) const;

	void load(std::string& file_path);
};

Problem::Problem()
{

}

Problem::Problem(std::string& file_path)
{
	load(file_path);
}

Problem::~Problem()
{

}

size_t Problem::get_n() const
{
	return n;
}

size_t Problem::get_m() const
{
	return m;
}

const matrix& Problem::get_M(void) const
{
	return M;
}

size_t Problem::get_cols() const
{
	return cols;
}

size_t Problem::get_rows() const
{
	return rows;
}

void Problem::load(std::string& file_path)
{
	std::ifstream file;
	file.open(file_path, std::ios_base::in);

	if (!file.is_open()) {
		std::runtime_error("Error al abrir el archivo!");
	}

	std::string line;
	std::getline(file, line);
	std::stringstream iss(line);

	// Leer n y m 
	iss >> n >> m;
	M = std::vector<std::vector<float>>(n);	
	// Leer c
	float aux;
	std::getline(file, line);
	iss = std::stringstream(line);
	while (iss >> aux) {
		c.push_back(aux);
	}

	// Leer la matriz
	for (size_t i = 0; i < n; ++i){
		// Leer una file
		std::getline(file, line);
		iss = std::stringstream(line);
		while (iss >> aux) {
			M.at(i).push_back(aux);
		}
	}

	rows = M.size();
	cols = M[0].size();

	if (cols != rows) {
		std::runtime_error("ERROR: numero de filas y columnas no coinciden");
	}
}

float Problem::cost(size_t a, size_t b) const
{
	float cost = 0.0f;

	// TODO: dijkstra o algo para sacar los nodos cuando no son conexos
	if (a < rows && b < rows)
		cost = M[a][b];

	return cost;
}

std::vector<size_t> Problem::sort_by_cost() const
{
	std::vector<size_t> indices(rows);
	std::iota(indices.begin(), indices.end(), 0);

	std::sort(indices.begin(), indices.end(),
			[this](size_t i, size_t j){return this->c.at(i) > this->c.at(j);});

	return indices;
}

float Problem::calc_cost(const std::vector<size_t> &indices) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (std::find(indices.begin(), indices.end(), i) == indices.end()) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (auto gw : indices) {
				float aux_cost = this->cost(i, gw) * c.at(i);
				if (aux_cost < instance_cost)
					instance_cost = aux_cost;
			}

			cost += instance_cost;
		}
	}

	return cost;
}

std::vector<size_t> Problem::routing(const std::vector<size_t>& gateways) const
{
	std::vector<size_t> routing(n);

	// Por cada nodo
	for (size_t id = 0; id < n; ++id) {
		// Si el nodo es un gateway, la salida sera el mismo
		if (std::find(gateways.begin(), gateways.end(), id) != gateways.end()) {
			routing[id] = id;
		} else {
			// Mirar que gateway tiene menos peso
			float cost = std::numeric_limits<float>::max();
			size_t gw = 0;
			for (auto aux_gw : gateways) {
				float aux_cost = this->cost(id, aux_gw);
				if (aux_cost < cost) {
					cost = aux_cost;
					gw = aux_gw;
				}
			}

			routing[id] = gw;
		}
	}

	return routing;
}

float greedy_criteria_1(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Obtener los indices ordenados por capacidad de almacenamiento
	std::vector<size_t> sorted_indices = problem.sort_by_cost();

	// Seleccionar dichos indices
	std::vector<size_t> indices(sorted_indices.begin(), sorted_indices.begin() + problem.get_m());

	// Guarda los gateways escogidos
	out_gateways = indices;

	// Obtiene el coste de esos gateways y lo devuelve
	return problem.calc_cost(indices);
}

float greedy_criteria_2(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Inicializar indices
	std::vector<size_t> gateways(problem.get_n());
	std::iota(gateways.begin(), gateways.end(), 0);


	for (size_t iteration = problem.get_n(); iteration > problem.get_m(); --iteration){
		float instance_cost = std::numeric_limits<float>::max();
		size_t gw_a_quitar = 0;

		// Quitar el gateway con el que se reduce mas el peso
		for (auto gw : gateways) {
			// Quitar el gateway actual
			std::vector<size_t> instance_gateways(gateways.begin(), gateways.end());
			instance_gateways.erase(std::find(instance_gateways.begin(), instance_gateways.end(), gw));

			// Calcula el coste y compara
			float aux_cost = problem.calc_cost(instance_gateways);
			if (aux_cost < instance_cost) {
				instance_cost = aux_cost;
				gw_a_quitar = gw;
			}
		}
		gateways.erase(std::find(gateways.begin(), gateways.end(), gw_a_quitar));
	}

	// Guardar los gateways escogidos
	out_gateways = gateways;

	return problem.calc_cost(gateways);
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		std::cerr << "ERROR: Numero de parametros incorrectos\n";
		exit(-1);
	}

	// Leer argumentos
	int argcnt = 1;
	std::string file_path;
	while (argcnt < argc) {
		if (std::string(argv[argcnt]).compare("-f") == 0) {
			if (argcnt + 1 < argc) {
				file_path = argv[argcnt+1];
				argcnt += 2;
			}
			else {
				std::cout << "Error: file not specified" << std::endl;
				exit(-1);
			}
		}
	}

	if (file_path.empty()) {
		std::cout << "Error: file not specified" << std::endl;
		exit(-1);
	}

	Problem problem(file_path);

	std::vector<size_t> indices_1, indices_2;
	float cost_1 = greedy_criteria_1(problem, indices_1), cost_2 = greedy_criteria_2(problem, indices_2);
	std::cout << "Greedy_criteria_1: " << cost_1 << std::endl;
	std::cout << "Greedy_criteria_2: " << cost_2 << std::endl;

	std::cout << "Routing: ";
	std::vector<size_t> routing;
	if (cost_1 < cost_2) {
		routing = problem.routing(indices_1);
	} else {
		routing = problem.routing(indices_2);
	}

	for (auto gw : routing) {
		std::cout << gw << " ";
	} std::cout << std::endl;

	return 0;
}	
