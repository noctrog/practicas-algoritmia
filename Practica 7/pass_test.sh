#!/bin/sh

for problem in ./tests/*.p
do
	echo -n $problem
	./met_greedy -f $problem > /tmp/result_test
	DIFF=$(diff /tmp/result_test $problem.sol)
	#echo $routing
	#echo $expected
	if [ "$DIFF" == "" ]; then
		echo ": ok"
	else 
		echo ": FAIL"
	fi
	#result_memizacion=$(echo "$output" | sed 1q | awk '{print $2}')
	#expected=$(cat $problem.sol | sed 1q | awk '{print $2}')
	#if [ $result_memizacion == $expected ]; then
		#echo $problem ": Ok"
	#fi
done
