for problem in ./tests/*.problem
do
	echo -n $problem
	output=$(./met --ignore_naive -f $problem)
	routing=$(echo "$output" | grep Routing)
	expected=$(cat $problem.sol | grep Routing)
	#echo $routing
	#echo $expected
	if [ "$routing" == "$expected" ]; then
		echo ": ok"
	else 
		echo ": FAIL"
	fi
	#result_memizacion=$(echo "$output" | sed 1q | awk '{print $2}')
	#expected=$(cat $problem.sol | sed 1q | awk '{print $2}')
	#if [ $result_memizacion == $expected ]; then
		#echo $problem ": Ok"
	#fi
done
