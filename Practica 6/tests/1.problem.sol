Naive: 108.5
Memoization: 108.5
Iterative (table): 108.5
Iterative (vector): 108.5
Routing: 1 1 1 4 4 4 6 
Estimated traffic: 108.5
Iterative table:
       0        -        - 
      32        0        - 
    69.5       32        0 
     192     69.5       32 
     244     82.5       45 
     305    108.5       71 
     419    203.5    108.5 
