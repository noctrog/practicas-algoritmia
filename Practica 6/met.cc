// Ramon Calvo Gonzalez 54205655Z

#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>
#include <limits>
#include <map>
#include <tuple>
#include <iomanip>
#include <cmath>

using result_key = std::tuple<size_t, size_t>;

struct ProgramState {
	bool table = false;
	bool ignore_naive = false;
} program_state;

struct Problem {
	size_t n, m;
	std::vector<float> c, d;
} problem;

// Guarda los resultados de las funciones
//std::map<result_key, std::pair<float, size_t>> one_gw_results;
std::map<result_key, float> one_gw_results;
std::map<result_key, float> met_naive_results;

// Guarda la tabla
std::map<result_key, float> table_results;

// Guarda las posiciones de los servidores
std::vector<size_t> routing_table;

inline void print_usage(void) { std::cout << "Usage" << std::endl << "met [-t] [--ignore_native] -f file" << std::endl;  }

float one_gw(size_t a, size_t n)
{
	float result = std::numeric_limits<float>::max();
	for(size_t i = a; i < n; ++i) {
		float left_cost = 0.0f, right_cost = 0.0f;
		for (size_t j = a; j < i; ++j) {
			left_cost += problem.c[j] * (problem.d[i] - problem.d[j]);
		}
		for (size_t j = n-1; j > i; --j) {
			right_cost += problem.c[j] * (problem.d[j] - problem.d[i]);
		} 
		result = (right_cost + left_cost < result) ? right_cost + left_cost : result;
	}

	return result;
}

std::pair<float, size_t>
one_gw_id(size_t a, size_t n)
{
	size_t id = 0;
	float cost = std::numeric_limits<float>::max();
	for(size_t i = a; i < n; ++i) {
		float left_cost = 0.0f, right_cost = 0.0f;
		for (size_t j = a; j < i; ++j) {
			left_cost += problem.c[j] * (problem.d[i] - problem.d[j]);
		}
		for (size_t j = n-1; j > i; --j) {
			right_cost += problem.c[j] * (problem.d[j] - problem.d[i]);
		} 
		if (right_cost + left_cost < cost) {
			cost = right_cost + left_cost;
			id = i;
		}
	}

	return {cost, id};
}

float met_naive(size_t m, size_t n)
{
	if (m == 1) {
		return one_gw(0, n);
	} else {
		float min_cost = std::numeric_limits<float>::max();
		for (size_t a = n; a >= m; --a) {
			float cost = met_naive(m-1, a) + one_gw(a, n);
			if (min_cost > cost) min_cost = cost;
		}
		return min_cost;
	}
} 

float met_memoizacion(size_t m, size_t n)
{
	if (m == 1) {
		auto it = one_gw_results.find(result_key(0, n));
		float res;
		// Si no se ha encontrado un elemento, calcularlo y guardarlo
		if (it == one_gw_results.end()) {
			res = one_gw(0, n);
			one_gw_results.insert({result_key(0, n), res});
		} else {	// Si se ha calculado, recuperarlo
			res = it->second;
		}

		return res;
	} else {
		float min_cost = std::numeric_limits<float>::max();
		for (size_t a = n; a >= m; --a) {
			float met_naive_cost, one_gw_cost;
			auto it = met_naive_results.find(result_key(m-1, a));
			// Si no se ha calculado previamente, calcularlo y guardarlo
			if (it == met_naive_results.end()) {
				met_naive_cost = met_memoizacion(m-1, a);
				met_naive_results.insert({result_key(m-1, a), met_naive_cost});
			} else {	// Si se ha calculado, recuperarlo
				met_naive_cost = it->second;
			}
			auto it2 = one_gw_results.find(result_key(a, n));
			// Si no se ha calculado previamente, calcularlo y guardarlo
			if (it2 == one_gw_results.end()) {
				one_gw_cost = one_gw(a, n);
				one_gw_results.insert({result_key(a, n), one_gw_cost});
			} else {	// Si se ha calculado, recuperarlo
				one_gw_cost = it2->second;
			}

			float cost = met_naive_cost + one_gw_cost;
			if (min_cost > cost) min_cost = cost;
		}
		return min_cost;
	}
}

float met_iterativo_tabla(size_t m, size_t n)
{
	// Recorrer la tabla de izquierda a derecha y de arriba a abajo
	for (size_t current_m = 1; current_m <= m; ++current_m) {
		for (size_t current_n = 1; current_n <= n; ++current_n) {
			// Si hay menos posiciones que puertas de enlace, no hay solucion
			if (current_m > current_n)
				continue;

			// Si el numero de puertas coinciden con el numero de servidores, la
			// solucion es 0
			if (current_m == current_n) {
				table_results.insert({result_key(current_m, current_n), 0.0f});
				continue;
			}
			
			// Caso base
			if (current_m == 1) {
				float res = one_gw(0, current_n);
				table_results.insert({result_key(current_m, current_n), res});
			}
			// Caso general
			else {
				float res = std::numeric_limits<float>::max();
				for (size_t a = current_n; a >= current_m-1; --a) {
					float cost = table_results.at(result_key( current_m-1, a )) + one_gw(a, current_n);
					if (res > cost) res = cost;
				}
				table_results.insert({result_key{current_m, current_n}, res});
			}
		}
	}

	return table_results.at(result_key( m, n ));
}

float met_iterativo_vector(size_t m, size_t n)
{
	// Inicializar los vectores auxiliares
	std::vector<float> S1(n, -1), S2(n, -1);

	// Recorrer la tabla de izquierda a derecha y de arriba a abajo
	for (size_t current_m = 1; current_m <= m; ++current_m) {
		// Intercambia los vectores
		std::swap(S1, S2);

		// Calcula los valores del nuevo vector
		for (size_t current_n = 1; current_n <= n; ++current_n) {
			// Si hay menos posiciones que puertas de enlace, no hay solucion
			if (current_m > current_n)
				continue;

			// Si el numero de puertas coinciden con el numero de servidores, la
			// solucion es 0
			if (current_m == current_n) {
				S2.at(current_n - 1) = 0.0f;
				continue;
			}
			
			// Caso base
			if (current_m == 1) {
				S2.at(current_n - 1) = one_gw(0, current_n);
			}
			// Caso general
			else {
				float res = std::numeric_limits<float>::max();
				for (size_t a = current_n; a >= current_m; --a) {
					float cost = S1.at(a-1) + one_gw(a, current_n);
					if (res > cost) res = cost;
				}
				S2.at(current_n - 1) = res;
			}
		}
	}

	return S2[S2.size()-1];
}

int main(int argc, char *argv[])
{
	std::ifstream input_file;

	// Se leen los argumentos
	bool bFileSpecified = false;
	int argcnt = 1;
	while (argcnt < argc) {
		if (std::string(argv[argcnt]).compare("-t") == 0) {
			program_state.table = true;
			argcnt++;
			continue;
		} else if (std::string(argv[argcnt]).compare("--ignore_naive") == 0) {
			program_state.ignore_naive = true;
			argcnt++;
			continue;
		} else if (std::string(argv[argcnt]).compare("-f") == 0) {
			bFileSpecified = true;
			if (argcnt + 1 < argc) {
				// Intentar leer el archivo
				input_file.open(argv[argcnt+1], std::ios_base::in);
				if (!input_file.is_open()) {
					// El archivo no se pudo abrir
					std::cout << "ERROR: can't open file: " << argv[argcnt+1] << std::endl;
					print_usage();
					exit(-1);
				}
				argcnt += 2;
				continue;
			} else {
				// ERROR: no se ha especificado el archivo
				std::cout << "ERROR: file not specified" << std::endl;
				print_usage();
				exit(-1);
			}
		} else {
			std::cout << "ERROR: unknown option " << argv[argcnt] << std::endl;
			print_usage();
			exit(-1);
		}
	}

	if (!bFileSpecified) {
		std::cout << "ERROR: file unspecified" << std::endl;
		print_usage();
		exit(-1);
	}

	// Leer los datos
	std::string line;
	std::getline(input_file, line);
	std::istringstream iss(line);
	float aux;
	// Leer n y m
	iss >> problem.n >> problem.m;
	// Lee c
	std::getline(input_file, line);
	iss = std::istringstream(line);
	while (iss >> aux) {
		problem.c.push_back(aux);
	}
	// Lee d
	std::getline(input_file, line);
	iss = std::istringstream(line);
	while (iss >> aux) {
		problem.d.push_back(aux);
	}

	// Reserva memoria para la tabla routing
	routing_table = std::vector<size_t>(problem.n, 0);

	// Los imprime 
	//std::cout << "n: " << problem.n << " m: " << problem.m << std::endl;
	//for (auto val : problem.c) {
		//std::cout << val << ", ";
	//} std::cout << std::endl;
	//for (auto val : problem.d) {
		//std::cout << val << ", ";
	//} std::cout << std::endl;

	// -----------------------------------------------------
	// ---------------------- INGENUA ----------------------
	// -----------------------------------------------------
	if (!program_state.ignore_naive) {
		float res_naive = met_naive(problem.m, problem.n);	
		std::cout << "Naive: " << res_naive << std::endl;
	}
	// -----------------------------------------------------
	// -------------------- MEMOIZACION --------------------
	// -----------------------------------------------------
	float res_memoizacion = met_memoizacion(problem.m, problem.n);
	std::cout << "Memoizacion: " << res_memoizacion << std::endl;
	// -----------------------------------------------------
	// -------------------- It. tabla   --------------------
	// -----------------------------------------------------
	float res_iterative_table = met_iterativo_tabla(problem.m, problem.n);
	std::cout << "Iterative (table): " << res_iterative_table << std::endl;
	// -----------------------------------------------------
	// ------------------- It. mejorado --------------------
	// -----------------------------------------------------
	std::cout << "Iterative (vector): " << met_iterativo_vector(problem.m, problem.n) <<  std::endl;

	// -----------------------------------------------------
	// ------------------- Routing --------------------
	// -----------------------------------------------------
	std::vector<size_t> gates(problem.m, 0);
	size_t ult = problem.n;
	for (size_t current_m = problem.m; current_m >= 1; --current_m) {
		float current_cost = table_results.at(result_key( current_m, ult ));

		for (size_t current_n = current_m - 1; current_n <= ult; ++current_n) {
			float cost = 0.0f;
			auto it = table_results.find(result_key( current_m - 1, current_n ));
			if (it != table_results.end()) {
				cost = it->second;
			}
			std::pair<float, size_t> gw = one_gw_id(current_n, ult);
			cost += gw.first;

			if (cost == current_cost) {
				ult = current_n;
				gates.at(current_m-1) = gw.second;
			}
		}
	}

	for (size_t i = 0; i < routing_table.size(); ++i) {
		float min_cost = std::numeric_limits<float>::max();
		for (size_t j = 0; j < gates.size(); ++j) {
			float cost = problem.c[i] * std::abs(problem.d[i] - problem.d[gates.at(j)]);
			if (cost < min_cost) {
				min_cost = cost;
				routing_table.at(i) = gates.at(j);
			}
		}
	}

	std::cout << "Routing: ";
	for (auto id : routing_table) {
		std::cout << id << " ";
	} std::cout << std::endl;

	std::cout << "Estimated traffic: " << res_memoizacion << std::endl;
	if (program_state.table) {
		std::cout << "Iterative table:\n¿?" << std::endl;

		for (size_t current_n = 1; current_n <= problem.n; ++current_n) {
			for (size_t current_m = 1; current_m <= problem.m; ++current_m) {
				// Busca elemento en la tabla guardada
				auto it = table_results.find(result_key(current_m, current_n));
				std::cout.width(8);
				if (it == table_results.end()) {
					std::cout << std::setw(8) << "-";
				} else {
					std::cout << std::setw(8) << it->second;
				}
			}
			std::cout << std::endl;
		}
	}
	
	return 0;
}	
