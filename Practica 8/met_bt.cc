// Ramon Calvo Gonzalez 54205655Z

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <numeric>
#include <chrono>

using matrix = std::vector<std::vector<float>>;

class Problem {
	size_t n, m;
	std::vector<float> c;
	size_t rows, cols;
	std::vector<std::vector<float>> M;
public:
	Problem();
	Problem(std::string& file_path);
	~Problem();

	size_t get_n(void) const;
	size_t get_m(void) const;
	const matrix& get_M(void) const;
	size_t get_cols(void) const;
	size_t get_rows(void) const;

	float cost(size_t a, size_t b) const;
	std::vector<size_t> sort_by_cost(void) const;
	float calc_cost(const std::vector<size_t>& indices) const;
	float calc_cost(const std::vector<bool>& indices) const;
	float calc_cost(const std::vector<bool>& indices, const std::vector<size_t>& order) const;
	std::vector<size_t> routing(const std::vector<size_t>& gateways) const;

	void load(std::string& file_path);
};

Problem::Problem()
{

}

Problem::Problem(std::string& file_path)
{
	load(file_path);
}

Problem::~Problem()
{

}

size_t Problem::get_n() const
{
	return n;
}

size_t Problem::get_m() const
{
	return m;
}

const matrix& Problem::get_M(void) const
{
	return M;
}

size_t Problem::get_cols() const
{
	return cols;
}

size_t Problem::get_rows() const
{
	return rows;
}

void Problem::load(std::string& file_path)
{
	std::ifstream file;
	file.open(file_path, std::ios_base::in);

	if (!file.is_open()) {
		std::runtime_error("Error al abrir el archivo!");
	}

	std::string line;
	std::getline(file, line);
	std::stringstream iss(line);

	// Leer n y m 
	iss >> n >> m;
	M = std::vector<std::vector<float>>(n);	
	// Leer c
	float aux;
	std::getline(file, line);
	iss = std::stringstream(line);
	while (iss >> aux) {
		c.push_back(aux);
	}

	// Leer la matriz
	for (size_t i = 0; i < n; ++i){
		// Leer una file
		std::getline(file, line);
		iss = std::stringstream(line);
		while (iss >> aux) {
			M.at(i).push_back(aux);
		}
	}

	rows = M.size();
	cols = M[0].size();

	if (cols != rows) {
		std::runtime_error("ERROR: numero de filas y columnas no coinciden");
	}
}

float Problem::cost(size_t a, size_t b) const
{
	float cost = 0.0f;

	if (a < rows && b < rows)
		cost = M[a][b];

	return cost;
}

std::vector<size_t> Problem::sort_by_cost() const
{
	std::vector<size_t> indices(rows);
	std::iota(indices.begin(), indices.end(), 0);

	std::sort(indices.begin(), indices.end(),
			[this](size_t i, size_t j){return this->c.at(i) > this->c.at(j);});

	return indices;
}

float Problem::calc_cost(const std::vector<size_t> &indices) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (std::find(indices.begin(), indices.end(), i) == indices.end()) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (auto gw : indices) {
				float aux_cost = this->cost(i, gw) * c.at(i);
				if (aux_cost < instance_cost)
					instance_cost = aux_cost;
			}

			cost += instance_cost;
		}
	}

	return cost;
}

float Problem::calc_cost(const std::vector<bool>& indices) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (!indices[i]) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (size_t j = 0; j < rows; ++j) {
				if (indices[j]) {
					float aux_cost = this->cost(i, j) * c.at(i);
					if (aux_cost < instance_cost) {
						instance_cost = aux_cost;
					}
				}
			}

			cost += instance_cost;
		}
	}

	return cost;
}

float Problem::calc_cost(const std::vector<bool>& indices, const std::vector<size_t>& order) const
{
	float cost = 0.0f;

	// Por cada nodo
	for (size_t i = 0; i < rows; ++i) {
		// Si el nodo no es un gateway
		if (!indices[order[i]]) {
			// Probar con cada gateway
			float instance_cost = std::numeric_limits<float>::max();
			for (size_t j = 0; j < rows; ++j) {
				if (indices[order[j]]) {
					float aux_cost = this->cost(order[i], order[j]) * c.at(order[i]);
					if (aux_cost < instance_cost) {
						instance_cost = aux_cost;
					}
				}
			}

			cost += instance_cost;
		}
	}

	return cost;
}
std::vector<size_t> Problem::routing(const std::vector<size_t>& gateways) const
{
	std::vector<size_t> routing(n);

	// Por cada nodo
	for (size_t id = 0; id < n; ++id) {
		// Si el nodo es un gateway, la salida sera el mismo
		if (std::find(gateways.begin(), gateways.end(), id) != gateways.end()) {
			routing[id] = id;
		} else {
			// Mirar que gateway tiene menos peso
			float cost = std::numeric_limits<float>::max();
			size_t gw = 0;
			for (auto aux_gw : gateways) {
				float aux_cost = this->cost(id, aux_gw);
				if (aux_cost < cost) {
					cost = aux_cost;
					gw = aux_gw;
				}
			}

			routing[id] = gw;
		}
	}

	return routing;
}

float greedy_criteria_1(const Problem& problem)
{
	// Obtener los indices ordenados por capacidad de almacenamiento
	std::vector<size_t> sorted_indices = problem.sort_by_cost();

	// Seleccionar dichos indices
	std::vector<size_t> indices(sorted_indices.begin(), sorted_indices.begin() + problem.get_m());

	// Obtiene el coste de esos gateways y lo devuelve
	return problem.calc_cost(indices);
}

//float greedy_criteria_2(const Problem& problem, std::vector<size_t>& orden)
//{
	//// Inicializar indices
	//std::vector<size_t> gateways(problem.get_n());
	//std::iota(gateways.begin(), gateways.end(), 0);

	//orden.clear();

	//float result = 0.0f;

	//for (size_t iteration = problem.get_n(); iteration > 1; --iteration){
		//float instance_cost = std::numeric_limits<float>::max();
		//size_t gw_a_quitar = 0;

		//// Quitar el gateway con el que se reduce mas el peso
		//for (auto gw : gateways) {
			//// Quitar el gateway actual
			//std::vector<size_t> instance_gateways(gateways.begin(), gateways.end());
			//instance_gateways.erase(std::find(instance_gateways.begin(), instance_gateways.end(), gw));

			//// Calcula el coste y compara
			//float aux_cost = problem.calc_cost(instance_gateways);
			//if (aux_cost < instance_cost) {
				//instance_cost = aux_cost;
				//gw_a_quitar = gw;
			//}
		//}
		//orden.push_back(gw_a_quitar);
		//gateways.erase(std::find(gateways.begin(), gateways.end(), gw_a_quitar));

		//if (iteration == problem.get_m() + 1) {
			//result = problem.calc_cost(gateways);
		//}
	//}

	//orden.push_back(gateways[0]);

	//return result;
//}

float greedy_criteria_1(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Obtener los indices ordenados por capacidad de almacenamiento
	std::vector<size_t> sorted_indices = problem.sort_by_cost();

	// Seleccionar dichos indices
	std::vector<size_t> indices(sorted_indices.begin(), sorted_indices.begin() + problem.get_m());

	// Guarda los gateways escogidos
	out_gateways = indices;

	// Obtiene el coste de esos gateways y lo devuelve
	return problem.calc_cost(indices);
}

float greedy_criteria_2(const Problem& problem, std::vector<size_t>& out_gateways)
{
	// Inicializar indices
	std::vector<size_t> gateways(problem.get_n());
	std::iota(gateways.begin(), gateways.end(), 0);


	for (size_t iteration = problem.get_n(); iteration > problem.get_m(); --iteration){
		float instance_cost = std::numeric_limits<float>::max();
		size_t gw_a_quitar = 0;

		// Quitar el gateway con el que se reduce mas el peso
		for (auto gw : gateways) {
			// Quitar el gateway actual
			std::vector<size_t> instance_gateways(gateways.begin(), gateways.end());
			instance_gateways.erase(std::find(instance_gateways.begin(), instance_gateways.end(), gw));

			// Calcula el coste y compara
			float aux_cost = problem.calc_cost(instance_gateways);
			if (aux_cost < instance_cost) {
				instance_cost = aux_cost;
				gw_a_quitar = gw;
			}
		}
		gateways.erase(std::find(gateways.begin(), gateways.end(), gw_a_quitar));
	}

	// Guardar los gateways escogidos
	out_gateways = gateways;

	return problem.calc_cost(gateways);
}

void bt(const Problem& problem,
	float& best_v,
	size_t& explored_nodes,
	const std::vector<size_t>& order,
	const size_t current_m,
	const size_t current_pos,
	std::vector<bool>& gateways,
	std::vector<bool>& current_gateways)
{
	// Si se ha pasado de una hoja, volver
	if (current_pos > problem.get_n()) {
		return;
	}

	explored_nodes++;

	// TODO: Si ya se han seleccionado m gateways, mirar el coste y podar
	if (current_m - (problem.get_n() - current_pos) == problem.get_m()) {
		for (size_t j = current_pos; j < problem.get_n(); ++j) {
			current_gateways[order[j]] = false;
		}
		//std::fill(current_gateways.begin() + current_pos, current_gateways.end(), false);
		float cost = problem.calc_cost(current_gateways, order);

		if (cost < best_v) {
			best_v = cost;
			gateways = current_gateways;

			std::cout << "c_pos: " << current_pos << "\t  c_m: " << current_m << "\t";
			for (auto i : current_gateways) {
				std::cout << i << " ";
			} std::cout << std::endl;
		}

		//std::fill(current_gateways.begin() + current_pos, current_gateways.end(), true);
		for (size_t j = current_pos; j < problem.get_n(); ++j) {
			current_gateways[order[j]] = true;
		}

		return;
	}

	// Calcular el coste del nodo actual
	float cost = problem.calc_cost(current_gateways, order);

	// Si el coste es mayor que la cota pesimista, podar
	if (cost > best_v)
		return;
	// Seguir explorando el arbol
	else {
		//for (size_t k = current_pos; k <= problem.get_n(); ++k) {
		//}
		current_gateways[order[ current_pos ]] = false;
		bt(problem, best_v, explored_nodes, order, current_m-1, current_pos+1, gateways, current_gateways);
		current_gateways[order[ current_pos ]] = true;
		bt(problem, best_v, explored_nodes, order, current_m, current_pos+1, gateways, current_gateways);
		//for (size_t k = current_pos)
	}
}

//void bt(const Problem& problem,
	//float& best_v,
	//size_t& explored_nodes,
	//const std::vector<size_t>& order,
	//const size_t current_m,
	//const size_t current_pos,
	//std::vector<bool>& gateways,
	//std::vector<bool>& current_gateways)
//{
	// Si se ha pasado de una hoja, volver evidentemente
	//if (current_pos > current_gateways.size()) {
		//return;
	//}

	//// Si hay menos gateways de los pedidos, volver tambien
	////if (current_m < problem.get_m()) {
		////return;
	////}

	//explored_nodes++;

	//// Si el numero de gateways es el pedido, probar a poner el resto 0
	////if (current_m == problem.get_m() && current_pos < problem.get_n()) {
		////std::fill(current_gateways.begin()+current_pos+1, current_gateways.end(), 0);
		////float cost = problem.calc_cost(current_gateways, order);

		////if (cost < best_v) {
			////best_v = cost;
			////gateways = current_gateways;
		////}

		////std::fill(current_gateways.begin()+current_pos+1, current_gateways.end(), 1);

		////return;
	////}


	//// Calcular coste de la disposición actual
	//float cost = problem.calc_cost(current_gateways, order);

	//// Si el coste es mayor que la cota pesimista, podar
	//if (cost > best_v) {
		//return;
	//}
	//// Si el nodo es una hoja y tiene los gateways pedidos, comparar resultado y guardar
	//else if (current_pos == current_gateways.size()) {
		////if (current_m == problem.get_m()){
			//if (cost < best_v) {
				//best_v = cost;
				//gateways = current_gateways;
				//return;
			//}
		////}
		////else 
			////return;
	//}
	//// Si no es una hoja, seguir explorando el arbol
	//else {
		//// Ir hacia la derecha es el mismo nodo, se puede hacer con un bucle
		//for (size_t k = current_pos; k <= problem.get_n(); ++k) {
			//// Rama izquierda (se cambia la posicion actual por un ordenador normal)
			//current_gateways[order[current_pos]] = 0;
			//bt(problem, best_v, explored_nodes, order, current_m-1, k+1, gateways, current_gateways);
			//current_gateways[order[current_pos]] = 1;
		//}
	//}
//}

void bt(const Problem& problem,
	float& best_v,
	std::vector<bool>& gateways,
	size_t& explored_nodes,
	const std::vector<size_t>& order)
{
	// Inicializar vector de servidores
	std::vector<bool> current_gateways(problem.get_n(), 1);

	// Llamar a la funcion recursiva
	bt(problem, best_v, explored_nodes, order, problem.get_n(), 0, gateways, current_gateways);
}

int main(int argc, char *argv[])
{
	if (argc != 3) {
		std::cerr << "ERROR: Numero de parametros incorrectos\n";
		exit(-1);
	}

	// Leer argumentos
	int argcnt = 1;
	std::string file_path;
	while (argcnt < argc) {
		if (std::string(argv[argcnt]).compare("-f") == 0) {
			if (argcnt + 1 < argc) {
				file_path = argv[argcnt+1];
				argcnt += 2;
			}
			else {
				std::cout << "Error: file not specified" << std::endl;
				exit(-1);
			}
		}
	}

	if (file_path.empty()) {
		std::cout << "Error: file not specified" << std::endl;
		exit(-1);
	}

	Problem problem(file_path);
	std::vector<size_t> indices1, indices2, indices;
	std::vector<size_t> order;

	auto start = std::chrono::high_resolution_clock::now();

	// Se calculan los dos metodos de voraz y nos quedamos con el mejor
	float greedy_1 = greedy_criteria_1(problem, indices1);
	float greedy_2 = greedy_criteria_2(problem, indices2);
	float greedy;
	if (greedy_1 < greedy_2) {
		greedy = greedy_1;
		indices = indices1;
	} else {
		greedy = greedy_2;
		indices = indices2;
	}

	order = problem.sort_by_cost();
	
	size_t explored_nodes = 0;
	std::vector<bool> gws;
	float backt = greedy;
	bt(problem, backt, gws, explored_nodes, order);
	auto end = std::chrono::high_resolution_clock::now();

	//std::cout << "Greedy 1: " << greedy_1 << std::endl;
	//std::cout << "Greedy 2: " << greedy_2 << std::endl;

	std::cout << "Backtracking: " << backt << std::endl;
	std::cout << "Gateways: ";
	if (greedy == backt) {
		for (auto gw : indices) {
			std::cout << gw << " ";
		}
	} else {
		for (size_t i = 0; i < gws.size(); ++i) {
			if (gws[order[i]]) {
				std::cout << order[i] << " ";
			} 
		} 
	}
	std::cout << std::endl;
	std::cout << "Explored nodes: " << explored_nodes << std::endl;
	std::cout << "CPU time (ms): " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()/1000.0 <<  std::endl;

	//std::vector<size_t> gws_prueba( { 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 22, 24, 25, 27, 28, 29 });
	//std::cout << problem.calc_cost(gws_prueba) << std::endl;
	
	//for (auto a : order) {
		//std::cout << a << " ";
	//} std::cout << std::endl;

	return 0;
}	
