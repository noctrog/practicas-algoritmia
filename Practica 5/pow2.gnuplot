# Ramón Calvo González; 54205655Z
set terminal png

# Imprimir grafica 1: Quicksort
set output "pow2.png"

set xlabel "n"
set ylabel "Millions of program steps"

set yrange [ 0: 400 ]

set title  "Costes"

plot "pow2.Msteps" using 1:2 with lines title "pow2_1" , "pow2.Msteps" using 1:3 with lines title "pow2_2" , "pow2.Msteps" using 1:4 with lines title "pow2_3"

