// Ramon Calvo Gonzalez 54205655Z

#include <iostream>
#include <vector>

const unsigned n_max = 9;
uint64_t CONTADOR = 0;

unsigned long pow2_1(unsigned);
unsigned long pow2_2(unsigned);
unsigned long pow2_3(unsigned);

// O(1)
unsigned long pow2_0(unsigned n)
{
	return (1UL << n);
}

unsigned long pow2_1(unsigned n)
{
	CONTADOR++;
	if (n == 0) {
		return 1UL;
	} else {
		return 2 * pow2_1(n-1);
	}
}

unsigned long pow2_2(unsigned n)
{
	CONTADOR++;
	if (n < 2){
		return 2UL;	
	} else {
		return pow2_2(n/2) * pow2_2(n/2 + (1 - (n % 2 == 0)));
	}
}

// Coste exponencial
unsigned long pow2_3(unsigned n)
{
	CONTADOR++;
	if (n == 0) {
		return 1UL;
	} else {
		return pow2_3(n-1) + pow2_3(n-1);
	}
}

void imprimir_cabecera(void)
{
	std::cout.width(8);
	std::cout << "Size";
	std::cout.width(12);
	std::cout << std::right << "pow2_1";
	std::cout.width(12);
	std::cout << std::right << "pow2_2";
	std::cout.width(12);
	std::cout << std::right << "pow2_3" << std::endl;
	for (int i = 0; i < (8 + 3 * 12); ++i) {
		std::cout << "-";
	}
	std::cout << std::endl;
}

int main(int argc, char *argv[])
{
	imprimir_cabecera();

	std::vector<uint64_t> contador_pow2_1;
	std::vector<uint64_t> contador_pow2_2;
	std::vector<uint64_t> contador_pow2_3;

	// Obtener las medidas con pow2_1
	for (unsigned n = 1; n <= n_max; ++n) {
		CONTADOR = 0;
		pow2_1(n);
		contador_pow2_1.push_back(CONTADOR);
	}

	// Obtener las medidas con pow2_2
	for (unsigned n = 1; n <= n_max; ++n) {
		CONTADOR = 0;
		pow2_2(n);
		contador_pow2_2.push_back(CONTADOR);
	}
	
	// Obtener las medidas con pow2_3
	for (unsigned n = 1; n <= n_max; ++n) {
		CONTADOR = 0;
		pow2_3(n);
		contador_pow2_3.push_back(CONTADOR);
	}
		
	// Imprimir resultados
	for (unsigned int i = 0; i < n_max; ++i) {
		std::cout.width(8);
		std::cout << std::right << (i+1);
		std::cout.width(12);
		std::cout << std::right << contador_pow2_1.at(i);
		std::cout.width(12);
		std::cout << std::right << contador_pow2_2.at(i);
		std::cout.width(12);
		std::cout << std::right << contador_pow2_3.at(i) << std::endl;
	}

	return 0;
}
