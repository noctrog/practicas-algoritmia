# Ramón Calvo González; 54205655Z
set terminal png

# Imprimir grafica 1: Quicksort
set output "quickSort.png"

set xlabel "size of arrays"
set ylabel "Millions of program steps"

set title  "Quicksort"

plot "qs-vs-hs.Msteps" using 1:2 with lines title "random arrays" , "qs-vs-hs.Msteps" using 1:4 with lines title "sorted arrays" , "qs-vs-hs.Msteps" using 1:6 with lines title "reverse-sorted arrays"

# Imprimir grafica 2: Quicksort
set output "heapSort.png"

set xlabel "size of arrays"
set ylabel "Millions of program steps"

set title  "Heapsort"

plot "qs-vs-hs.Msteps" using 1:3 with lines title "random arrays" , "qs-vs-hs.Msteps" using 1:5 with lines title "sorted arrays" , "qs-vs-hs.Msteps" using 1:7 with lines title "reverse-sorted arrays"

# Imprimir grafica 3: Comparacion con arrays randoms
set output "qs-vs-hs-RA.png"

set xlabel "size of arrays"
set ylabel "Millions of program steps"

set title  "Quicksort vs Heapsort (random arrays)"

y(x)=a*x*log(x)+b
fitFunction_qs="Fitting Quicksort time values to f(n) = n*log(n)"
fit y(x) "qs-vs-hs.Msteps" using 1:2 via a,b

z(x)=a*x*log(x)+b
fitFunction_hs="Fitting Heapsort time values to f(n) = n*log(n)"
fit z(x) "qs-vs-hs.Msteps" using 1:3 via a,b

plot "qs-vs-hs.Msteps" using 1:2 with lines title "Quicksort" , "qs-vs-hs.Msteps" using 1:3 with lines title "Heapsort", y(x) title fitFunction_qs, z(x) title fitFunction_hs

# Imprimir grafica 4: Comparacion con arrays orenados
set output "qs-vs-hs-SA.png"

set xlabel "size of arrays"
set ylabel "Millions of program steps"

set title  "Quicksort vs Heapsort (sorted arrays)"

y(x)=a*x*log(x)+b
fitFunction_qs="Fitting Quicksort time values to f(n) = n*log(n)"
fit y(x) "qs-vs-hs.Msteps" using 1:4 via a,b

z(x)=a*x*log(x)+b
fitFunction_hs="Fitting Heapsort time values to f(n) = n*log(n)"
fit z(x) "qs-vs-hs.Msteps" using 1:5 via a,b

plot "qs-vs-hs.Msteps" using 1:4 with lines title "Quicksort" , "qs-vs-hs.Msteps" using 1:5 with lines title "Heapsort", y(x) title fitFunction_qs, z(x) title fitFunction_hs

# Imprimir grafica 5: Comparacion con arrays ordenados inversos
set output "qs-vs-hs-RSA.png"

set xlabel "size of arrays"
set ylabel "Millions of program steps"

set title  "Quicksort vs Heapsort (reverse-sorted arrays)"

y(x)=a*x*log(x)+b
fitFunction_qs="Fitting Quicksort time values to f(n) = n*log(n)"
fit y(x) "qs-vs-hs.Msteps" using 1:6 via a,b

z(x)=a*x*log(x)+b
fitFunction_hs="Fitting Heapsort time values to f(n) = n*log(n)"
fit z(x) "qs-vs-hs.Msteps" using 1:6 via a,b

plot "qs-vs-hs.Msteps" using 1:6 with lines title "Quicksort" , "qs-vs-hs.Msteps" using 1:7 with lines title "Heapsort", y(x) title fitFunction_qs, z(x) title fitFunction_hs
##########################################################################
#Testing different analytical functions to determine which best fits:

#set output "qs-vs-hs_and_Fit-Function.png"
#set title "Bubble Sort: Exact time complexity and Fit function"


#------------------------------------------------------------------------

# Testing 3 different functions:
#Uncomment the function to be tested and comment the rest:

##Fit to a linear function: 
#y(x)=a*x + b 
#fitFunction="Fitting Bubblesort time values to f(n) = n+k"
#fit y(x) "qs-vs-hs.CPUtimes" using 1:2 via a,b


##Fit to a (n log n + k) function: 
#y(x)=a*x*log(x) + b 
#fitFunction="Fitting Bubblesort time values to f(n) = n log(n)+k"
#fit y(x) "qs-vs-hs.CPUtimes" using 1:2 via a,b


##Fit to a cuadratic function: 
#y(x)=a*x*x + b*x + c
#fitFunction="Fitting Bubblesort time values to f(n) = n²+n+k"
#fit y(x) "qs-vs-hs.CPUtimes" using 1:2 via a,b,c

#------------------------------------------------------------------------


#Finally draw both functions (CPU times of sample and the one that best fits) on one graph
#plot "qs-vs-hs.CPUtimes" using 1:2 with lines title "CPU times of sample", y(x) title fitFunction  


