/* Nombre: Ramón Calvo González DNI: 54205655Z */
/*
   ALGIR. 2019-20
   Práctica 2 de laboratorio: Empirical analysis by means of program-steps account of two sorting algorithms: Middle-Quicksort and Heapsort.

   Objetivos de la práctica (además de los especificados en la práctica anterior):
   - Conocer los algoritmos de ordenación de vectores Quicksort y Heapsort
   - Saber utilizar el método de la cuenta de pasos de manera empírica para obtener la complejidad temporal de un algoritmo.
   - Comprender la necesidad de generar un número (significativo desde el punto de vista estadístico) de instancias aleatorias para el cálculo de la complejidad temporal promedio cuando el algoritmo presenta caso mejor y peor.
   - Entender que el tiempo que tarda un algoritmo en resolver un problema ((o la memoria que requiere) )puede depender del tipo de instancias del problema que se le presentan.
   */
#include <unistd.h>
#include <iostream>
#include <vector>
#include <array>
#include <cmath>

enum class Array : uint32_t {
	RANDOM,
	SORTED,
	REVERSE_SORTED
};

const float PASOS_A_MILLONES = 1 / 1000000.0f;
const uint32_t n_tests = 6;

static uint64_t CONTADOR_Q = 0;
static uint64_t CONTADOR_H = 0;

//--------------------------------------------------------------
// Middle Quick Sort
void 
middle_QuickSort(int * v, long left, long right){
	long i,j;
	int pivot; 
	if (left<right){
		CONTADOR_Q++;
		i=left; j=right;
		pivot=v[(i+j)/2];
		do{
			while (v[i]<pivot) { i++; CONTADOR_Q++; }
			while (v[j]>pivot) { j--; CONTADOR_Q++; }
			if (i<=j){ 
				CONTADOR_Q++;
				std::swap(v[i], v[j]);
				i++; j--;
			}
		}while (i<=j);
		if (left < j)  middle_QuickSort(v,left,j);
		if (i < right) middle_QuickSort(v,i,right);
	}
}

//--------------------------------------------------------------                
//HEAPSORT
// Procedure sink used by Heapsort algorith
// Sink an element (indexed by i) in a tree to keep the heap property. n is the size of the heap. 

void sink(int *v, size_t n, size_t i)
{
	size_t largest;
	size_t l, r; //left and right child

	do{
		CONTADOR_H++;
		largest = i;  // Initialize largest as root
		l = 2*i + 1;  // left = 2*i + 1
		r = 2*i + 2;  // right = 2*i + 2

		// Is left child larger than root?
		if (l < n && v[l] > v[largest])
			largest = l;

		// Is right child larger than largest so far
		if (r < n && v[r] > v[largest])
			largest = r;

		// If largest is still root then the process is done
		if (largest == i) break;

		// If not, swap new largest with current node i and repeat the process with childs. 
		std::swap(v[i], v[largest]);
		i=largest;  
	} while (true);
}

//--------------------------------------------------------------                
// Heapsort algorithm (ascending sorting)
void heapSort(int *v, size_t n)
{
	// Build a MAX-HEAP with the input array
	for (size_t i = n / 2 - 1; i >= 0; i--){
		CONTADOR_H++;
		sink(v, n, i);
		if (i==0) break; //as size_t is unsigned type
	}	

	// A this point we have a HEAP on the input array, let's take advantage of this to sort the array:
	// One by swap the first element, which is the largest (max-heap), with the last element of the vector and rebuild heap by sinking the new placed element on the begin of vector.  
	for (size_t i=n-1; i>0; i--)
	{
		CONTADOR_H++;
		// Move current root to the end.
		std::swap(v[0], v[i]);
		// Now largest element is at the end but do not know if the element just moved to the beginning is well placed, so a sink process is required.
		// Note that, at the same time, the HEAP size is reduced one element by one.
		sink(v, i, 0);
		// The procedure ends when HEAP has only one element, the lower of the input array
	}
}

void imprimir_cabecera()
{
	// Imprimir la cabecera
	std::cout << "QUICKSORT VERSUS HEAPSORT" << std::endl;
	std::cout << "Average processing Msteps (millions of program steps)" << std::endl;
	std::cout << "Number of samples (arrays of integer): 30" << std::endl << std::endl;
	std::cout << "\t\tRANDOM ARRAYS\t    SORTED ARRAYS     REVERSE SORTED ARRAYS" << std::endl;
	std::cout << "\t    -------------------  -------------------  ---------------------" << std::endl;
	std::cout.width(8);
	std::cout << std::right << "Size";
	std::cout << "    QuickSort  HeapSort  QuickSort  HeapSort  QuickSort   HeapSort" << std::endl;
	std::cout << "===========================================================================" << std::endl;
}

std::vector<int> generar_array(uint32_t n, Array type) 
{
	std::vector<int> arr(std::pow(2, n), 0.0f);

	switch(type) {
		case Array::RANDOM:
			for (size_t i = 0; i < arr.size(); ++i) {
				arr.at(i) = rand() % 100000;
			}
			break;
		case Array::SORTED:
			for (size_t i = 0; i < arr.size(); ++i) {
				arr.at(i) = i;
			}
			break;
		case Array::REVERSE_SORTED:
			for (size_t i = 0; i < arr.size(); ++i) {
				arr.at(i) = (arr.size() - i);
			}
			break;
		default:
			break;
	}

	return arr;
}

std::array<float, 2> obtener_prueba(uint32_t size, Array tipo_array)
{
	std::array<float, 2> resultados;

	std::vector<float> resultados_quicksort;
	std::vector<float> resultados_heapsort;

	for (uint32_t i = 0; i < 30; ++i) {
		std::vector<int> arr_q = generar_array(size, tipo_array);
		std::vector<int> arr_h(arr_q);

		CONTADOR_H = 0; CONTADOR_Q = 0;

		middle_QuickSort(arr_q.data(), 0, arr_q.size() - 1);
		heapSort(arr_h.data(), arr_h.size());

		resultados_quicksort.push_back(CONTADOR_Q);
		resultados_heapsort.push_back(CONTADOR_H);
	}

	float sum_q = 0.0f, sum_h = 0.0f;

	for (uint32_t i = 0; i < resultados_quicksort.size(); ++i) {
		sum_q += resultados_quicksort.at(i);
		sum_h += resultados_heapsort.at(i);
	}

	float mean_q = sum_q / resultados_quicksort.size();
	float mean_h = sum_h / resultados_heapsort.size();

	resultados.at(0) = mean_q;
	resultados.at(1) = mean_h;

	return resultados;
}

std::vector<float> obtener_resultados_para_fila(uint32_t size)
{
	std::vector<float> resultados;
	std::array<float, 2> aux;

	// ---- RANDOM -----
	aux = obtener_prueba(size, Array::RANDOM);
	resultados.push_back(aux.at(0) * PASOS_A_MILLONES);
	resultados.push_back(aux.at(1) * PASOS_A_MILLONES);

	// ---- SORTED ----
	aux = obtener_prueba(size, Array::SORTED);
	resultados.push_back(aux.at(0) * PASOS_A_MILLONES);
	resultados.push_back(aux.at(1) * PASOS_A_MILLONES);

	// ---- REVERSED ----
	aux = obtener_prueba(size, Array::REVERSE_SORTED);
	resultados.push_back(aux.at(0) * PASOS_A_MILLONES);
	resultados.push_back(aux.at(1) * PASOS_A_MILLONES);

	return resultados;
}

void imprimir_resultados(std::array<std::vector<float>, n_tests> resultados)
{
	std::cout.precision(3);
	for (uint32_t i = 0; i < n_tests; ++i) {
		std::cout.width(8);
		std::cout << std::fixed << static_cast<uint32_t>( std::pow(2, (i + 15) ));
		for (auto result : resultados.at(i)) {
			std::cout.width(11);
			std::cout << std::fixed << result;
		}

		std::cout << std::endl;
	}
}

int main(int argc, char *argv[])
{
	imprimir_cabecera();

	srand(time(NULL));

	std::array<std::vector<float>, n_tests> resultados;

	for (uint32_t size = 15; size < 21; ++size) {
		resultados.at( size - 15 ) = obtener_resultados_para_fila(size);
	}

	imprimir_resultados(resultados);

	return 0;
}
