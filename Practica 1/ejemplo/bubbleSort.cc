/*
ALGORITMIA - ING. ROBÓTICA. Curso 2019-20
Práctica 1: Empirical analysis of Quicksort average-time complexity.

Objetivos de la práctica:

- Refrescar el lenguaje de programación y el uso de algunas herramientas: compilador, make, editor, etc.
- Saber obtener de manera empírica la complejidad temporal de un algoritmo, generando instancias aleatorias y cronometrando el tiempo que tarda en resolverlas.
- Utilizar la herramienta Gnuplot para dibujar gráficas
- Conocer el tipo de datos "size_t" para representar el tamaño de los objetos, útil para evitar desbordamientos de los tipos compuestos; "size_t" se adapta al tamaño del mayor objeto representable.

*/

#include <unistd.h>
#include <iostream>
#include <math.h>

using namespace std;

//--------------------------------------------------------------
// Bubble sort basic algorithm
void 
bubbleSort(int * v, size_t size){
int aux;

for (size_t i = 1; i < size; i++)
        for (size_t j = 0; j < size - i; j++)
                if (v[j]>v[j+1]){ /*swap*/
                        aux = v[j]; 
						v[j] = v[j+1]; 
						v[j+1] = aux;
                }
}  
//--------------------------------------------------------------                


int
main(void){

	int * v;
  
	size_t size;

	srand(getpid());

	cout << "BubbleSort CPU times in milliseconds:"
		<< endl
		<< "Size \t CPU time (ms.)"
		<< endl
		<< "----------------------------"
		<< endl;

	for (int exp = 12; exp <= 17; exp++){
		size = (size_t) powl(2,exp);
		v = new int [size];
		if (v){
			cout << size << "\t\t" << std::flush;

			for (size_t j = 0; j < size; j++) 
				v[j] = rand(); 

            auto start = clock();
			bubbleSort(v,size);
            auto end = clock();
            cout << 1000.0 * (end-start) / CLOCKS_PER_SEC  << endl;

			for (size_t i = 1; i < size; i++)
				if (v[i] < v[i-1]){ 
					cerr << "Panic, array not sorted! " << i << endl; 
					exit(1);			
				}

			delete v; 
		}
		else {
			cerr << "Error, not enough memory!" << endl;
			exit (1);  
		}
	}
}


