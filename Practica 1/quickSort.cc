<!DOCTYPE html>

<html  dir="ltr" lang="es" xml:lang="es">
<head>
    <title>(33723_1_2019-20)</title>
    <link rel="shortcut icon" href="https://moodle2019-20.ua.es/moodle/theme/image.php/boost/theme/1547117835/favicon" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="moodle, (33723_1_2019-20)" />
<link rel="stylesheet" type="text/css" href="https://moodle2019-20.ua.es/moodle/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple.css" /><script id="firstthemesheet" type="text/css">/** Required in order to fix style inclusion problems in IE with YUI **/</script><link rel="stylesheet" type="text/css" href="https://moodle2019-20.ua.es/moodle/theme/styles.php/boost/1547117835_1/all" />
<script type="text/javascript">
//<![CDATA[
var M = {}; M.yui = {};
M.pageloadstarttime = new Date();
M.cfg = {"wwwroot":"https:\/\/moodle2019-20.ua.es\/moodle","sesskey":"uaoZPgDlBh","themerev":"1547117835","slasharguments":1,"theme":"boost","iconsystemmodule":"core\/icon_system_fontawesome","jsrev":"1547117835","admin":"admin","svgicons":true,"usertimezone":"Europa\/Madrid","contextid":38909,"developerdebug":true};var yui1ConfigFn = function(me) {if(/-skin|reset|fonts|grids|base/.test(me.name)){me.type='css';me.path=me.path.replace(/\.js/,'.css');me.path=me.path.replace(/\/yui2-skin/,'/assets/skins/sam/yui2-skin')}};
var yui2ConfigFn = function(me) {var parts=me.name.replace(/^moodle-/,'').split('-'),component=parts.shift(),module=parts[0],min='-min';if(/-(skin|core)$/.test(me.name)){parts.pop();me.type='css';min=''}
if(module){var filename=parts.join('-');me.path=component+'/'+module+'/'+filename+min+'.'+me.type}else{me.path=component+'/'+component+'.'+me.type}};
YUI_config = {"debug":true,"base":"https:\/\/moodle2019-20.ua.es\/moodle\/lib\/yuilib\/3.17.2\/","comboBase":"https:\/\/moodle2019-20.ua.es\/moodle\/theme\/yui_combo.php?","combine":true,"filter":"RAW","insertBefore":"firstthemesheet","groups":{"yui2":{"base":"https:\/\/moodle2019-20.ua.es\/moodle\/lib\/yuilib\/2in3\/2.9.0\/build\/","comboBase":"https:\/\/moodle2019-20.ua.es\/moodle\/theme\/yui_combo.php?","combine":true,"ext":false,"root":"2in3\/2.9.0\/build\/","patterns":{"yui2-":{"group":"yui2","configFn":yui1ConfigFn}}},"moodle":{"name":"moodle","base":"https:\/\/moodle2019-20.ua.es\/moodle\/theme\/yui_combo.php?m\/1547117835\/","combine":true,"comboBase":"https:\/\/moodle2019-20.ua.es\/moodle\/theme\/yui_combo.php?","ext":false,"root":"m\/1547117835\/","patterns":{"moodle-":{"group":"moodle","configFn":yui2ConfigFn}},"filter":"DEBUG","modules":{"moodle-core-actionmenu":{"requires":["base","event","node-event-simulate"]},"moodle-core-blocks":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification"]},"moodle-core-checknet":{"requires":["base-base","moodle-core-notification-alert","io-base"]},"moodle-core-chooserdialogue":{"requires":["base","panel","moodle-core-notification"]},"moodle-core-dock":{"requires":["base","node","event-custom","event-mouseenter","event-resize","escape","moodle-core-dock-loader","moodle-core-event"]},"moodle-core-dock-loader":{"requires":["escape"]},"moodle-core-dragdrop":{"requires":["base","node","io","dom","dd","event-key","event-focus","moodle-core-notification"]},"moodle-core-event":{"requires":["event-custom"]},"moodle-core-formchangechecker":{"requires":["base","event-focus","moodle-core-event"]},"moodle-core-handlebars":{"condition":{"trigger":"handlebars","when":"after"}},"moodle-core-languninstallconfirm":{"requires":["base","node","moodle-core-notification-confirm","moodle-core-notification-alert"]},"moodle-core-lockscroll":{"requires":["plugin","base-build"]},"moodle-core-maintenancemodetimer":{"requires":["base","node"]},"moodle-core-notification":{"requires":["moodle-core-notification-dialogue","moodle-core-notification-alert","moodle-core-notification-confirm","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-core-notification-dialogue":{"requires":["base","node","panel","escape","event-key","dd-plugin","moodle-core-widget-focusafterclose","moodle-core-lockscroll"]},"moodle-core-notification-alert":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-confirm":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-exception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-notification-ajaxexception":{"requires":["moodle-core-notification-dialogue"]},"moodle-core-popuphelp":{"requires":["moodle-core-tooltip"]},"moodle-core-tooltip":{"requires":["base","node","io-base","moodle-core-notification-dialogue","json-parse","widget-position","widget-position-align","event-outside","cache-base"]},"moodle-core_availability-form":{"requires":["base","node","event","event-delegate","panel","moodle-core-notification-dialogue","json"]},"moodle-backup-backupselectall":{"requires":["node","event","node-event-simulate","anim"]},"moodle-backup-confirmcancel":{"requires":["node","node-event-simulate","moodle-core-notification-confirm"]},"moodle-course-categoryexpander":{"requires":["node","event-key"]},"moodle-course-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-course-coursebase","moodle-course-util"]},"moodle-course-formatchooser":{"requires":["base","node","node-event-simulate"]},"moodle-course-management":{"requires":["base","node","io-base","moodle-core-notification-exception","json-parse","dd-constrain","dd-proxy","dd-drop","dd-delegate","node-event-delegate"]},"moodle-course-modchooser":{"requires":["moodle-core-chooserdialogue","moodle-course-coursebase"]},"moodle-course-util":{"requires":["node"],"use":["moodle-course-util-base"],"submodules":{"moodle-course-util-base":{},"moodle-course-util-section":{"requires":["node","moodle-course-util-base"]},"moodle-course-util-cm":{"requires":["node","moodle-course-util-base"]}}},"moodle-form-dateselector":{"requires":["base","node","overlay","calendar"]},"moodle-form-passwordunmask":{"requires":[]},"moodle-form-shortforms":{"requires":["node","base","selector-css3","moodle-core-event"]},"moodle-form-showadvanced":{"requires":["node","base","selector-css3"]},"moodle-question-chooser":{"requires":["moodle-core-chooserdialogue"]},"moodle-question-preview":{"requires":["base","dom","event-delegate","event-key","core_question_engine"]},"moodle-question-qbankmanager":{"requires":["node","selector-css3"]},"moodle-question-searchform":{"requires":["base","node"]},"moodle-availability_completion-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_date-form":{"requires":["base","node","event","io","moodle-core_availability-form"]},"moodle-availability_grade-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_group-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_grouping-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-availability_profile-form":{"requires":["base","node","event","moodle-core_availability-form"]},"moodle-mod_assign-history":{"requires":["node","transition"]},"moodle-mod_forum-subscriptiontoggle":{"requires":["base-base","io-base"]},"moodle-mod_quiz-autosave":{"requires":["base","node","event","event-valuechange","node-event-delegate","io-form"]},"moodle-mod_quiz-dragdrop":{"requires":["base","node","io","dom","dd","dd-scroll","moodle-core-dragdrop","moodle-core-notification","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-base","moodle-mod_quiz-util-page","moodle-mod_quiz-util-slot","moodle-course-util"]},"moodle-mod_quiz-modform":{"requires":["base","node","event"]},"moodle-mod_quiz-questionchooser":{"requires":["moodle-core-chooserdialogue","moodle-mod_quiz-util","querystring-parse"]},"moodle-mod_quiz-quizbase":{"requires":["base","node"]},"moodle-mod_quiz-repaginate":{"requires":["base","event","node","io","moodle-core-notification-dialogue"]},"moodle-mod_quiz-toolboxes":{"requires":["base","node","event","event-key","io","moodle-mod_quiz-quizbase","moodle-mod_quiz-util-slot","moodle-core-notification-ajaxexception"]},"moodle-mod_quiz-util":{"requires":["node","moodle-core-actionmenu"],"use":["moodle-mod_quiz-util-base"],"submodules":{"moodle-mod_quiz-util-base":{},"moodle-mod_quiz-util-slot":{"requires":["node","moodle-mod_quiz-util-base"]},"moodle-mod_quiz-util-page":{"requires":["node","moodle-mod_quiz-util-base"]}}},"moodle-message_airnotifier-toolboxes":{"requires":["base","node","io"]},"moodle-filter_glossary-autolinker":{"requires":["base","node","io-base","json-parse","event-delegate","overlay","moodle-core-event","moodle-core-notification-alert","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-filter_mathjaxloader-loader":{"requires":["moodle-core-event"]},"moodle-editor_atto-editor":{"requires":["node","transition","io","overlay","escape","event","event-simulate","event-custom","node-event-html5","node-event-simulate","yui-throttle","moodle-core-notification-dialogue","moodle-core-notification-confirm","moodle-editor_atto-rangy","handlebars","timers","querystring-stringify"]},"moodle-editor_atto-plugin":{"requires":["node","base","escape","event","event-outside","handlebars","event-custom","timers","moodle-editor_atto-menu"]},"moodle-editor_atto-menu":{"requires":["moodle-core-notification-dialogue","node","event","event-custom"]},"moodle-editor_atto-rangy":{"requires":[]},"moodle-report_eventlist-eventfilter":{"requires":["base","event","node","node-event-delegate","datatable","autocomplete","autocomplete-filters"]},"moodle-report_loglive-fetchlogs":{"requires":["base","event","node","io","node-event-delegate"]},"moodle-gradereport_grader-gradereporttable":{"requires":["base","node","event","handlebars","overlay","event-hover"]},"moodle-gradereport_history-userselector":{"requires":["escape","event-delegate","event-key","handlebars","io-base","json-parse","moodle-core-notification-dialogue"]},"moodle-tool_capability-search":{"requires":["base","node"]},"moodle-tool_lp-dragdrop-reorder":{"requires":["moodle-core-dragdrop"]},"moodle-tool_monitor-dropdown":{"requires":["base","event","node"]},"moodle-assignfeedback_editpdf-editor":{"requires":["base","event","node","io","graphics","json","event-move","event-resize","transition","querystring-stringify-simple","moodle-core-notification-dialog","moodle-core-notification-alert","moodle-core-notification-exception","moodle-core-notification-ajaxexception"]},"moodle-atto_accessibilitychecker-button":{"requires":["color-base","moodle-editor_atto-plugin"]},"moodle-atto_accessibilityhelper-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_align-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_bold-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_charmap-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_clear-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_collapse-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_count-button":{"requires":["io","json-parse","moodle-editor_atto-plugin"]},"moodle-atto_emoticon-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_equation-button":{"requires":["moodle-editor_atto-plugin","moodle-core-event","io","event-valuechange","tabview","array-extras"]},"moodle-atto_html-beautify":{},"moodle-atto_html-button":{"requires":["promise","moodle-editor_atto-plugin","moodle-atto_html-beautify","moodle-atto_html-codemirror","event-valuechange"]},"moodle-atto_html-codemirror":{"requires":["moodle-atto_html-codemirror-skin"]},"moodle-atto_image-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_indent-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_italic-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_link-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_managefiles-usedfiles":{"requires":["node","escape"]},"moodle-atto_media-button":{"requires":["moodle-editor_atto-plugin","moodle-form-shortforms"]},"moodle-atto_noautolink-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_orderedlist-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_recordrtc-button":{"requires":["moodle-editor_atto-plugin","moodle-atto_recordrtc-recording"]},"moodle-atto_recordrtc-recording":{"requires":["moodle-atto_recordrtc-button"]},"moodle-atto_rtl-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_strike-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_subscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_superscript-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_table-button":{"requires":["moodle-editor_atto-plugin","moodle-editor_atto-menu","event","event-valuechange"]},"moodle-atto_title-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_underline-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_undo-button":{"requires":["moodle-editor_atto-plugin"]},"moodle-atto_unorderedlist-button":{"requires":["moodle-editor_atto-plugin"]}}},"gallery":{"name":"gallery","base":"https:\/\/moodle2019-20.ua.es\/moodle\/lib\/yuilib\/gallery\/","combine":true,"comboBase":"https:\/\/moodle2019-20.ua.es\/moodle\/theme\/yui_combo.php?","ext":false,"root":"gallery\/1547117835\/","patterns":{"gallery-":{"group":"gallery"}}}},"modules":{"core_filepicker":{"name":"core_filepicker","fullpath":"https:\/\/moodle2019-20.ua.es\/moodle\/lib\/javascript.php\/1547117835\/repository\/filepicker.js","requires":["base","node","node-event-simulate","json","async-queue","io-base","io-upload-iframe","io-form","yui2-treeview","panel","cookie","datatable","datatable-sort","resize-plugin","dd-plugin","escape","moodle-core_filepicker","moodle-core-notification-dialogue"]},"core_comment":{"name":"core_comment","fullpath":"https:\/\/moodle2019-20.ua.es\/moodle\/lib\/javascript.php\/1547117835\/comment\/comment.js","requires":["base","io-base","node","json","yui2-animation","overlay","escape"]},"mathjax":{"name":"mathjax","fullpath":"https:\/\/cdnjs.cloudflare.com\/ajax\/libs\/mathjax\/2.7.2\/MathJax.js?delayStartupUntil=configured"}}};
M.yui.loader = {modules: {}};

//]]>
</script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body  id="page-enrol-index" class="format-topics  path-enrol dir-ltr lang-es yui-skin-sam yui3-skin-sam moodle2019-20-ua-es--moodle pagelayout-incourse course-2071 context-38909 category-41 drawer-open-left">

<div id="page-wrapper">

    <div>
    <a class="sr-only sr-only-focusable" href="#maincontent">Saltar a contenido principal</a>
</div><script type="text/javascript" src="https://moodle2019-20.ua.es/moodle/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple.js"></script><script type="text/javascript" src="https://moodle2019-20.ua.es/moodle/lib/javascript.php/1547117835/lib/javascript-static.js"></script>
<script type="text/javascript">
//<![CDATA[
document.body.className += ' jsenabled';
//]]>
</script>



    <nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" aria-label="Navegación del sitio">
    
            <div data-region="drawer-toggle" class="d-inline-block mr-3">
                <button aria-expanded="true" aria-controls="nav-drawer" type="button" class="btn nav-link float-sm-left mr-1 btn-secondary" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav"><i class="icon fa fa-bars fa-fw " aria-hidden="true"  aria-label=""></i><span class="sr-only">Panel lateral</span></button>
            </div>
    
            <a href="https://moodle2019-20.ua.es/moodle" class="navbar-brand has-logo
                    ">
                    <span class="logo d-none d-sm-inline">
                        <img src="https://moodle2019-20.ua.es/moodle/pluginfile.php/1/core_admin/logocompact/0x70/1547117835/logo_negre_UACloud_Campus_Virtual%282%29.png" alt="|">
                    </span>
                <span class="site-name d-none d-md-inline">|</span>
            </a>
    
            <ul class="navbar-nav d-none d-md-flex">
                <!-- custom_menu -->
                <li class="dropdown nav-item">
    <a class="dropdown-toggle nav-link" id="drop-down-5d7fe4b1017475d7fe4b1004b34" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" aria-controls="drop-down-menu-5d7fe4b1017475d7fe4b1004b34">
        Español - Internacional ‎(es)‎
    </a>
    <div class="dropdown-menu" role="menu" id="drop-down-menu-5d7fe4b1017475d7fe4b1004b34" aria-labelledby="drop-down-5d7fe4b1017475d7fe4b1004b34">
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ca" title="Català ‎(ca)‎">Català ‎(ca)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=de" title="Deutsch ‎(de)‎">Deutsch ‎(de)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=en" title="English ‎(en)‎">English ‎(en)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=es" title="Español - Internacional ‎(es)‎">Español - Internacional ‎(es)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=fr" title="Français ‎(fr)‎">Français ‎(fr)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ro" title="Română ‎(ro)‎">Română ‎(ro)‎</a>
                <a class="dropdown-item" role="menuitem" href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ar" title="عربي ‎(ar)‎">عربي ‎(ar)‎</a>
    </div>
</li>
                <!-- page_heading_menu -->
                
            </ul>
            <ul class="nav navbar-nav ml-auto">
                <div class="d-none d-lg-block">
                
            </div>
                <!-- navbar_plugin_output -->
                <li class="nav-item">
                
                </li>
                <!-- user_menu -->
                <li class="nav-item d-flex align-items-center">
                    <div class="usermenu"><span class="login">En este momento está usando el acceso para invitados (<a href="https://moodle2019-20.ua.es/moodle/login/index.php">Acceder</a>)</span></div>
                </li>
            </ul>
            <!-- search_box -->
    </nav>
    

    <div id="page" class="container-fluid">
        <header id="page-header" class="row">
    <div class="col-12 pt-3 pb-3">
        <div class="card ">
            <div class="card-body ">
                <div class="d-flex">
                    <div class="mr-auto">
                        <div class="page-context-header"><div class="page-header-headings"><h1>2019-20_ALGORITMIA_33723</h1></div></div>
                    </div>

                </div>
                <div class="d-flex flex-wrap">
                    <div id="page-navbar">
                        <nav role="navigation">
    <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="https://moodle2019-20.ua.es/moodle/" >Página Principal</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="https://moodle2019-20.ua.es/moodle/course/index.php" >Cursos</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="https://moodle2019-20.ua.es/moodle/course/index.php?categoryid=41" >Ciencias</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="https://moodle2019-20.ua.es/moodle/course/view.php?id=2071" title="2019-20_ALGORITMIA_33723">(33723_1_2019-20)</a>
                </li>
                <li class="breadcrumb-item">Opciones de matriculación</li>
    </ol>
</nav>
                    </div>
                    <div class="ml-auto d-flex">
                        
                    </div>
                    <div id="course-header">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

        <div id="page-content" class="row pb-3">
            <div id="region-main-box" class="col-12">
                <section id="region-main" >

                    <span class="notifications" id="user-notifications"></span>
                    <div role="main"><span id="maincontent"></span><h2>Opciones de matriculación</h2><div class="box generalbox info py-3"><div class="coursebox clearfix" data-courseid="2071" data-type="1"><div class="info"><h3 class="coursename"><a class="" href="https://moodle2019-20.ua.es/moodle/course/view.php?id=2071">2019-20_ALGORITMIA_33723</a></h3><div class="moreinfo"></div></div><div class="content"><div class="courseimage"><img src="https://moodle2019-20.ua.es/moodle/pluginfile.php/38909/course/overviewfiles/logo.png" /></div><ul class="teachers"><li>Profesor: <a href="https://moodle2019-20.ua.es/moodle/user/view.php?id=386&amp;course=1">MAXIMILIANO SAIZ NOEDA</a></li><li>Profesor: <a href="https://moodle2019-20.ua.es/moodle/user/view.php?id=13121&amp;course=1">Jose Luis Verdu Mas</a></li></ul></div></div></div><div id="notice" class="box generalbox py-3">Los invitados no pueden entrar a este curso. Por favor acceda con sus datos.</div><div class="continuebutton">
    <form method="get" action="https://moodle2019-20.ua.es/moodle/login/index.php" >
        <button type="submit" class="btn btn-primary"
            id="single_button5d7fe4b1004b37"
            title=""
            >Continuar</button>
    </form>
</div></div>
                    
                    

                </section>
            </div>
        </div>
    </div>
    
    <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " aria-hidden="false" tabindex="-1">
        <nav class="list-group">
            <a class="list-group-item list-group-item-action " href="https://moodle2019-20.ua.es/moodle/course/view.php?id=2071" data-key="coursehome" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="60" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" >
                <div class="m-l-0">
                    <div class="media">
                        <span class="media-left">
                            <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"  aria-label=""></i>
                        </span>
                        <span class="media-body ">(33723_1_2019-20)</span>
                    </div>
                </div>
            </a>
        </nav>
        <nav class="list-group m-t-1">
            <a class="list-group-item list-group-item-action " href="https://moodle2019-20.ua.es/moodle/" data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="1" data-type="1" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="0" data-hidden="0" data-preceedwithhr="0" >
                <div class="m-l-0">
                    <div class="media">
                        <span class="media-left">
                            <i class="icon fa fa-home fa-fw " aria-hidden="true"  aria-label=""></i>
                        </span>
                        <span class="media-body ">Página Principal</span>
                    </div>
                </div>
            </a>
            <a class="list-group-item list-group-item-action " href="https://moodle2019-20.ua.es/moodle/calendar/view.php?view=month&amp;course=2071" data-key="calendar" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="60" data-nodetype="0" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="1">
                <div class="m-l-0">
                    <div class="media">
                        <span class="media-left">
                            <i class="icon fa fa-calendar fa-fw " aria-hidden="true"  aria-label=""></i>
                        </span>
                        <span class="media-body ">Calendario</span>
                    </div>
                </div>
            </a>
        </nav>
    </div>
    <footer id="page-footer" class="py-3 bg-dark text-light">
        <div class="container">
            <div id="course-footer"></div>
    
    
            <div class="logininfo">En este momento está usando el acceso para invitados (<a href="https://moodle2019-20.ua.es/moodle/login/index.php">Acceder</a>)</div>
            <div class="tool_usertours-resettourcontainer"></div>
            <div class="homelink"><a href="https://moodle2019-20.ua.es/moodle/course/view.php?id=2071">(33723_1_2019-20)</a></div>
            <nav class="nav navbar-nav d-md-none">
                    <ul class="list-unstyled pt-3">
                                        <li><a href="#" title="Idioma">Español - Internacional ‎(es)‎</a></li>
                                    <li>
                                        <ul class="list-unstyled ml-3">
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ca" title="Català ‎(ca)‎">Català ‎(ca)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=de" title="Deutsch ‎(de)‎">Deutsch ‎(de)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=en" title="English ‎(en)‎">English ‎(en)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=es" title="Español - Internacional ‎(es)‎">Español - Internacional ‎(es)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=fr" title="Français ‎(fr)‎">Français ‎(fr)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ro" title="Română ‎(ro)‎">Română ‎(ro)‎</a></li>
                                                            <li><a href="https://moodle2019-20.ua.es/moodle/enrol/index.php?id=2071&amp;lang=ar" title="عربي ‎(ar)‎">عربي ‎(ar)‎</a></li>
                                        </ul>
                                    </li>
                    </ul>
            </nav>
            <div class="tool_dataprivacy"><a href="https://moodle2019-20.ua.es/moodle/admin/tool/dataprivacy/summary.php">Resumen de retención de datos</a></div><a href="https://download.moodle.org/mobile?version=2018120301.04&amp;lang=es&amp;iosappid=633359593&amp;androidappid=com.moodle.moodlemobile">Descargar la app para dispositivos móviles</a>
            
<a href="https://si.ua.es/moodle">Tutorial Moodle UA</a><script type="text/javascript">
//<![CDATA[
var require = {
    baseUrl : 'https://moodle2019-20.ua.es/moodle/lib/requirejs.php/1547117835/',
    // We only support AMD modules with an explicit define() statement.
    enforceDefine: true,
    skipDataMain: true,
    waitSeconds : 0,

    paths: {
        jquery: 'https://moodle2019-20.ua.es/moodle/lib/javascript.php/1547117835/lib/jquery/jquery-3.2.1.min',
        jqueryui: 'https://moodle2019-20.ua.es/moodle/lib/javascript.php/1547117835/lib/jquery/ui-1.12.1/jquery-ui.min',
        jqueryprivate: 'https://moodle2019-20.ua.es/moodle/lib/javascript.php/1547117835/lib/requirejs/jquery-private'
    },

    // Custom jquery config map.
    map: {
      // '*' means all modules will get 'jqueryprivate'
      // for their 'jquery' dependency.
      '*': { jquery: 'jqueryprivate' },
      // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
      '*': { process: 'core/first' },

      // 'jquery-private' wants the real jQuery module
      // though. If this line was not here, there would
      // be an unresolvable cyclic dependency.
      jqueryprivate: { jquery: 'jquery' }
    }
};

//]]>
</script>
<script type="text/javascript" src="https://moodle2019-20.ua.es/moodle/lib/javascript.php/1547117835/lib/requirejs/require.js"></script>
<script type="text/javascript">
//<![CDATA[
require(['core/first'], function() {
;
require(["media_videojs/loader"], function(loader) {
    loader.setUp(function(videojs) {
        videojs.options.flash.swf = "https://moodle2019-20.ua.es/moodle/media/player/videojs/videojs/video-js.swf";
videojs.addLanguage("es",{
 "Play": "Reproducción",
 "Play Video": "Reproducción Vídeo",
 "Pause": "Pausa",
 "Current Time": "Tiempo reproducido",
 "Duration Time": "Duración total",
 "Remaining Time": "Tiempo restante",
 "Stream Type": "Tipo de secuencia",
 "LIVE": "DIRECTO",
 "Loaded": "Cargado",
 "Progress": "Progreso",
 "Fullscreen": "Pantalla completa",
 "Non-Fullscreen": "Pantalla no completa",
 "Mute": "Silenciar",
 "Unmute": "No silenciado",
 "Playback Rate": "Velocidad de reproducción",
 "Subtitles": "Subtítulos",
 "subtitles off": "Subtítulos desactivados",
 "Captions": "Subtítulos especiales",
 "captions off": "Subtítulos especiales desactivados",
 "Chapters": "Capítulos",
 "You aborted the media playback": "Ha interrumpido la reproducción del vídeo.",
 "A network error caused the media download to fail part-way.": "Un error de red ha interrumpido la descarga del vídeo.",
 "The media could not be loaded, either because the server or network failed or because the format is not supported.": "No se ha podido cargar el vídeo debido a un fallo de red o del servidor o porque el formato es incompatible.",
 "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducción de vídeo se ha interrumpido por un problema de corrupción de datos o porque el vídeo precisa funciones que su navegador no ofrece.",
 "No compatible source was found for this media.": "No se ha encontrado ninguna fuente compatible con este vídeo."
});

    });
});;

require(['jquery'], function($) {
    $('#single_select5d7fe4b1004b33').change(function() {
        var ignore = $(this).find(':selected').attr('data-ignore');
        if (typeof ignore === typeof undefined) {
            $('#single_select_f5d7fe4b1004b32').submit();
        }
    });
});
;

require(['jquery'], function($) {
    $('#single_select5d7fe4b1004b36').change(function() {
        var ignore = $(this).find(':selected').attr('data-ignore');
        if (typeof ignore === typeof undefined) {
            $('#single_select_f5d7fe4b1004b35').submit();
        }
    });
});
;

require(['theme_boost/loader']);
require(['theme_boost/drawer'], function(mod) {
    mod.init();
});
;
require(["core/notification"], function(amd) { amd.init(38909, []); });;
require(["core/log"], function(amd) { amd.setConfig({"level":"trace"}); });;
require(["core/page_global"], function(amd) { amd.init(); });
});
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
M.str = {"moodle":{"lastmodified":"\u00daltima modificaci\u00f3n","name":"Nombre","error":"Error","info":"Informaci\u00f3n","yes":"S\u00ed","no":"No","cancel":"Cancelar","confirm":"Confirmar","areyousure":"\u00bfEst\u00e0 seguro?","closebuttontitle":"Cerrar","unknownerror":"Error desconocido"},"repository":{"type":"Tipo","size":"Tama\u00f1o","invalidjson":"Cadena JSON no v\u00e1lida","nofilesattached":"No se han adjuntado archivos","filepicker":"Selector de archivos","logout":"Salir","nofilesavailable":"No hay archivos disponibles","norepositoriesavailable":"Lo sentimos, ninguno de sus repositorios actuales puede devolver archivos en el formato solicitado.","fileexistsdialogheader":"El archivo existe","fileexistsdialog_editor":"Un archivo con el mismo nombre ya se ha adjuntado al texto que est\u00e1 editando.","fileexistsdialog_filemanager":"Un archivo con ese nombre ya ha sido adjuntado","renameto":"Cambiar el nombre a \"{$a}\"","referencesexist":"Existen {$a} archivos de alias\/atajos que emplean este archivo como su or\u00edgen","select":"Seleccionar"},"admin":{"confirmdeletecomments":"Est\u00e1 a punto de eliminar comentarios, \u00bfest\u00e1 seguro?","confirmation":"Confirmaci\u00f3n"}};
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
(function() {Y.use("moodle-filter_mathjaxloader-loader",function() {M.filter_mathjaxloader.configure({"mathjaxconfig":"MathJax.Hub.Config({\r\n    config: [\"Accessible.js\", \"Safe.js\"],\r\n    errorSettings: { message: [\"!\"] },\r\n    skipStartupTypeset: true,\r\n    messageStyle: \"none\"\r\n});\r\n","lang":"es"});
});
M.util.help_popups.setup(Y);
 M.util.js_pending('random5d7fe4b1004b38'); Y.on('domready', function() { M.util.js_complete("init");  M.util.js_complete('random5d7fe4b1004b38'); });
})();
//]]>
</script>

        </div>
    </footer></div>

</body>
</html>