/* Ramon Calvo Gonzalez 54205655Z */

#include <cctype>
#include <cstdlib>
//#include <unistd.h>
#include <cmath>
#include <vector>
#include <ctime>

#include <iostream>

//--------------------------------------------------------------
// Middle QuickSort
void middle_QuickSort(int * v, long left, long right){
	long i,j;
	int pivot,aux; 
	if (left<right){
		i=left; j=right;
		pivot=v[(i+j)/2];
		do{
			while (v[i]<pivot) i++;
			while (v[j]>pivot) j--;
			if (i<=j) {
				aux=v[i]; v[i]=v[j]; v[j]=aux;
				i++; j--;
			}
		}while (i<=j);
		if (left<j)  middle_QuickSort(v,left,j);
		if (i<right) middle_QuickSort(v,i,right);
	}
}

int main(void){

	int * v;

	std::size_t size;


	std::cout << "QuickSort CPU times in milliseconds:"
		<< std::endl
		<< "Size \t CPU time (ms.)"
		<< std::endl
		<< "----------------------------"
		<< std::endl;

	srand(time(NULL));

	for (int exp = 15; exp < 21; exp++){
		std::vector<float> vec;
		size = (size_t) powl(2,exp);
		std::cout << size << "\t\t" << std::flush;
		for (size_t k = 0; k < 30; ++k) {
			v = new int [size];
			if (v){

				for (size_t j = 0; j < size; j++) 
					v[j] = rand(); 

				auto start = clock();
				middle_QuickSort(v, 0, size-1);
				auto end = clock();
				vec.push_back(1000.0 * (end-start) / CLOCKS_PER_SEC);

				for (size_t i = 1; i < size; i++)
					if (v[i] < v[i-1]){ 
						std::cerr << "Panic, array not sorted! " << i << std::endl; 
						exit(1);			
					}

				delete v; 
			}
			else {
				std::cerr << "Error, not enough memory!" << std::endl;
				exit (1);  
			}
		}

		float mean_time = 0.0f;
		for (auto k : vec)
			mean_time += k;
		mean_time /= vec.size();
		std::cout << mean_time << std::endl;
	}
}
